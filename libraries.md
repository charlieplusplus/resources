# 📚 Libraries

## Angular
- [ngx-quill](https://github.com/KillerCodeMonkey/ngx-quill): Angular (>=2) components for the Quill Rich Text Editor.
- [Angular Calendar](https://github.com/mattlewis92/angular-calendar): An open-source library for including month/week/day calendar components.
- [@angular/cdk/drag-drop](https://material.angular.io/cdk/drag-drop/overview)

##### Component Libraries
- [NG-ZORRO](https://ng.ant.design/docs/introduce/en)
- [Lightning](https://ng-lightning.github.io/ng-lightning/#/get-started)
- [Angular Material](https://material.angular.io/)


## 🗺️ Geospatial + Maps
- [Turf.js](http://turfjs.org/): Advanced geospatial analysis for browsers and Node.js.


## Icons
- [Teenyicons](https://teenyicons.com/)

## Interactions
- [Hotkey](https://github.com/github/hotkey): Trigger an action on an element with a keyboard shortcut.

## Misc
- [Color Thief](https://lokeshdhakar.com/projects/color-thief/): Grab the color palette from an image using just Javascript. Works in the browser and Node.
